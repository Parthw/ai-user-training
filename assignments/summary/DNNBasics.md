**Deep Neural Network(DNN)**

> A deep neural network is a neural network which consists of more than one hidden layer.Its also called as fully connected layer because each neuron is connected to every other neuron of next layer and sometimes it is also called as multilayer perceptron because it is a an artificial neural network consisting of many perceptrons connected together.


- A neural network  is a network of neurons or nodes.
- Neurons : Thing that holds a number between 0 and 1
- The number inside the neuron is called as activation number.
There are such 28*28 neuron grids = 784
Therefore, 784 makes the first layer

*Edge Detection* assigning wieghts to different connection from first layer to next .
These wieghts and activation number together sum up to give result


*Gradient Descent*
Gradient of the value gives the direction of steepest ascent

A loss function is a function that computes the cost of the network by comparing the predicted and labelled values.*

An optimizer function is a function that computes the performance of the network such that weights and biases get altered which results in decreasing log-likelihood loss function.
The most likely used optimizer are Gradient Descent, Stochastic Gradient Descent  in which , the data is shuffled initially and then Gradient Descent is applied to a mini-batch of data so that the performance of the neural network increases.

*Backpropagation *is an algorithm which is used for the training of deep neural network for the purpose of giving correct outputs.
It is the essence of neural net training. It is the method of fine-tuning the weights of a neural net based on the error rate obtained in the previous epoch (i.e., iteration). Proper tuning of the weights allows you to reduce error rates and to make the model reliable by increasing its generalization.