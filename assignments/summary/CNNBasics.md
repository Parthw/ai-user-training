**CNN**
- A convolutional neural network (CNN or ConvNet) is one of the most popular algorithms for deep learning, a type of machine learning in which a model learns to perform classification tasks directly from images, video, text, or sound.

CNNs can be retrained for new recognition tasks, enabling you to build on pre-existing networks.
CNNs eliminate the need for manual feature extraction—the features are learned directly by the CNN
*3 Layers of CNN are:*

1. Convolution puts the input images through a set of convolutional filters, each of which activates certain features from the images.In this layer,a small grid of parameters is chosen from the actual desired output class and it is convolved through the grid of parameters in the input. This is called a kernel and it is optimizable therefore it helps recognize new features in an image no matter where in the picture it is located.

2. Rectified linear unit (ReLU) allows for faster and more effective training by mapping negative values to zero and maintaining positive values. This is sometimes referred to as activation, because only the activated features are carried forward into the next layer.

3. Pooling A pooling layer performs the operation which reduces the in-plane dimensionality of the feature maps in order to reduce the variance which arises due to small shifts and disorientations, and decrease the number of extra learnable parameters. It makes the feature maps one dimensional i.e. flattened.

*Terms used in CNN:*
Flattening - Coverts feature matrices to a vector.
Filter - It is a matrix containing values corresponding to a particular feature.
Activation Function - Decides which neurons to activate. Commonly used - ReLu
Image - It is the input matrix to the CNN from which features are matched with the filter.
Zero padding - Amount of zeroes added to image border to ensure convolution of egde pixels of image.
Kernel Size - Size of sliding window in pixels. Smaller and odd size is prefered.

**Summary**

1.Give input image into convolution layer
2. Choose parameters, filters with strides, padding. 
3. Perform convolution and apply ReLU activation. Perform pooling
4. Add as many convolutional layers until satisfied
5. Flatten the output and feed into a FC Layer
6. Output class using an activation function and classifies images.